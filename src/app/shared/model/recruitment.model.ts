import {TypeNotifyNew} from './typeNotifyNew';
import {CandidateModel} from './candidate.model';

export interface RecruitmentModel {
  id?: number;
  title?: string;
  company?: string;
  description?: string;
  content?: string;
  type?: TypeNotifyNew;
  createdDate?: Date;
  expiredDate?: Date;
  logo?: string;
  address?: string;
  amount?: number;
  phoneNumber?: string;
  email?: string;
  salary?: string;
  display?: boolean;
  candidates?: CandidateModel[];
}
