import {RoleModel} from './role.model';
import {Gender} from './gender';
import {StudentModel} from './student.model';
import {EmployeeModel} from './employee.model';

export interface UserModel {
  id?: number;
  usernameOrEmail?: string;
  password?: string;
  fullname?: string;
  username?: string;
  email?: string;
  roles?: RoleModel[];
  phoneNumber?: string;
  birthday?: Date;
  active?: boolean;
  key?: string;
  gender?: Gender;
  student?: StudentModel;
  employee?: EmployeeModel;
}
