export interface ImageModel {
  id?: number;
  caption?: string;
  path?: string;
}
