export interface StudentModel {
  id?: number;
  cardId?: string;
  studentClass?: string;
  faculty?: string;
  facebook?: string;
}
