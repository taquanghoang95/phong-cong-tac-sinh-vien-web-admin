import {TypeNotifyNew} from './typeNotifyNew';
import {LinkModel} from './link.model';

export interface NewsModel {
  id?: number;
  title?: string;
  description?: string;
  image?: string;
  content?: string;
  type?: TypeNotifyNew;
  createdDate?: Date;
  links?: LinkModel[];
  display?: boolean;
}
