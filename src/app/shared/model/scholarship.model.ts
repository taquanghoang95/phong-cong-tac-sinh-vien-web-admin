import {LinkModel} from './link.model';
import {ImageModel} from './image.model';

export interface ScholarshipModel {
  id?: number;
  title?: string;
  description?: string;
  type?: string;
  amount?: number;
  level?: string;
  information?: string;
  links?: LinkModel[];
  images?: ImageModel[];
  display?: boolean;
  createdDate?: Date;
  expiredDate?: Date;
}
