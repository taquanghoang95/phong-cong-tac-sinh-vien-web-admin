import {RecruitmentModel} from './recruitment.model';

export interface CandidateModel {
  id?: number;
  fullname?: string;
  email?: string;
  phone?: string;
  recruitment?: RecruitmentModel;
}
