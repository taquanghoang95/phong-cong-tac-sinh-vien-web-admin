export interface LinkModel {
  id?: number;
  caption?: string;
  path?: string;
}
