import {LinkModel} from './link.model';
import {ImageModel} from './image.model';
import {TypeNotifyNew} from './typeNotifyNew';

export interface NotifyModel {
  id?: number;
  title?: string;
  description?: string;
  content?: string;
  type?: TypeNotifyNew;
  createdDate?: Date;
  links?: LinkModel[];
  images?: ImageModel[];
  display?: boolean;
}
