export interface EmployeeModel {
  id?: number;
  department?: string;
  position?: string;
}
