import { InjectionToken } from '@angular/core';

export interface AppConfig {
  API_ROOT: string;
}

export const APP_CONFIG = new InjectionToken<AppConfig>('app.config');
