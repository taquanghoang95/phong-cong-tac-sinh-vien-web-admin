import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HeaderComponent} from './header/header.component';
import {MenuComponent} from './menu/menu.component';
import {RouterModule} from '@angular/router';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {ApiInterceptor} from './http-interceptor/api-interceptor';
import {NotifyService} from './services/notify.service';
import {HttpService} from './services/http.service';
import {AuthGuard} from './authentication/auth-guard.service';
import {AuthenticationService} from './services/authentication.service';
import {SweetAlertService} from './services/sweet-alert.service';
import {ScholarshipService} from './services/scholarship.service';
import {RecruitmentService} from './services/recruitment.service';
import {NewsService} from './services/news.service';

@NgModule({
  declarations: [HeaderComponent, MenuComponent],
  exports: [
    HeaderComponent,
    MenuComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    HttpClientModule
  ],
  providers: [
    NotifyService,
    ScholarshipService,
    RecruitmentService,
    NewsService,
    HttpService,
    AuthGuard,
    AuthenticationService,
    SweetAlertService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ApiInterceptor,
      multi: true
    }
  ]
})
export class CoreModule {
}
