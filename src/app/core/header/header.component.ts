import {Component, OnInit} from '@angular/core';
import {AuthenticationService} from '../services/authentication.service';
import {UserService} from '../services/user.service';
import {UserModel} from '../../shared/model/user.model';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  public hideUserSetting = true;
  public user: UserModel;

  constructor(public authenticationService: AuthenticationService,
              public userService: UserService) {
  }

  ngOnInit() {
    this.getUser();
  }

  private getUser(): void {
    if (this.authenticationService.isLogin()) {
      this.userService.getUser(this.authenticationService.getUserId()).subscribe(res => this.user = res);
    }
  }

}
