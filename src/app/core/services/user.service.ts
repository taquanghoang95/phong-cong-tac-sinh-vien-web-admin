import {Injectable} from '@angular/core';
import {HttpService} from './http.service';
import {Observable} from 'rxjs';
import {UserModel} from '../../shared/model/user.model';

@Injectable()
export class UserService {

  private url = '/api/users';

  constructor(private httpService: HttpService) {
  }

  public addUser(userModel: UserModel): Observable<any> {
    return this.httpService.postWithToken('/api/auth/register-employee', userModel);
  }

  public getUsers(): Observable<UserModel[]> {
    return this.httpService.getWithToken(this.url);
  }

  public getUser(id: number): Observable<UserModel> {
    return this.httpService.getWithToken(this.url + '/' + id);
  }

  public deleteUser(id: number): Observable<any> {
    return this.httpService.deleteWithToken(this.url + '/' + id);
  }
}
