import {Injectable} from '@angular/core';
import {HttpService} from './http.service';
import {Observable} from 'rxjs';
import {RecruitmentModel} from '../../shared/model/recruitment.model';

@Injectable()
export class RecruitmentService {
  private url = '/api/recruitment';

  constructor(private httpService: HttpService) {
  }

  public addRecruitment(recruitmentModel: RecruitmentModel): Observable<any> {
    return this.httpService.postWithToken(this.url, recruitmentModel);
  }

  public getRecruitment(): Observable<RecruitmentModel[]> {
    return this.httpService.getWithToken(this.url);
  }

  public getRecruitmentById(id: string): Observable<RecruitmentModel> {
    return this.httpService.getWithToken(this.url + '/' + id);
  }

  public deleteRecruitment(id: number): Observable<any> {
    return this.httpService.deleteWithToken(this.url + '/' + id);
  }

  public updateDisplayRecruitment(id: number, display: boolean): Observable<any> {
    return this.httpService.postWithToken(this.url + '/update?id=' + id + '&display=' + display, {});
  }
}
