import {Injectable} from '@angular/core';
import {NotifyModel} from '../../shared/model/notify.model';
import {Observable} from 'rxjs';
import {HttpService} from './http.service';

@Injectable()
export class NotifyService {

  private url = '/api/notifies';

  constructor(private httpService: HttpService) {
  }

  public addNotify(notifyModel: NotifyModel): Observable<any> {
    return this.httpService.postWithToken(this.url, notifyModel);
  }

  public getNotifies(): Observable<NotifyModel[]> {
    return this.httpService.getWithToken(this.url);
  }

  public deleteNotify(id: number): Observable<any> {
    return this.httpService.deleteWithToken(this.url + '/' + id);
  }

  public updateDisplayNotify(id: number, display: boolean): Observable<any> {
    return this.httpService.postWithToken(this.url + '/update?id=' + id + '&display=' + display, {});
  }
}
