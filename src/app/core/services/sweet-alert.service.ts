import { Injectable } from '@angular/core';
import Swal from 'sweetalert2';

@Injectable()
export class SweetAlertService {

  public Swal(args = {}) {
    return Swal(args);
  }

  public alertConfirm(option) {
    const baseOptions = {
      title: 'Are you sure?',
      showCancelButton: true,
      type: 'warning',
      customClass: 'customer-sweetaleart',
      heightAuto: false
    };
    return this.Swal((<any>Object).assign(baseOptions, option));
  }

  public alertSuccess(content) {
    const baseOptions = {
      title: 'Success!',
      html: content,
      type: 'success',
      timer: 5000,
      customClass: 'customer-sweetaleart',
      heightAuto: false
    };
    return this.Swal((<any>Object).assign(baseOptions)).catch(Swal.noop);
  }

  public alertInfo(content) {
    const baseOptions = {
      title: 'Information!',
      html: content,
      type: 'info',
      timer: 5000,
      customClass: 'customer-sweetaleart',
      heightAuto: false
    };
    return this.Swal((<any>Object).assign(baseOptions));
  }

  public alertError(content) {
    const baseOptions = {
      title: 'Error!',
      html: content,
      type: 'error',
      timer: 5000,
      customClass: 'customer-sweetaleart',
      heightAuto: false
    };
    return this.Swal((<any>Object).assign(baseOptions)).catch(Swal.noop);
  }

  public alertWarning(title, content) {
    const baseOptions = {
      title: title,
      html: content,
      type: 'warning',
      width: '670px',
      confirmButtonText: 'OK',
      customClass: 'customer-sweetaleart',
      heightAuto: false
    };
    return this.Swal((<any>Object).assign(baseOptions)).catch(Swal.noop);
  }

  public alertQuestion(option) {
    const baseOptions = {
      showCancelButton: true,
      type: 'question',
      customClass: 'customer-sweetaleart',
      heightAuto: false
    };
    return this.Swal((<any>Object).assign(baseOptions, option));
  }

  public alertToastWarning(content) {
    const baseOptions = {
      html: content,
      type: 'warning',
      timer: 5000,
      toast: true,
      position: 'top',
      customClass: 'customer-sweetaleart'
    };
    return this.Swal((<any>Object).assign(baseOptions)).catch(Swal.noop);
  }

  public alertToastSuccess(content) {
    const baseOptions = {
      html: content,
      type: 'success',
      timer: 5000,
      toast: true,
      position: 'top',
      customClass: 'customer-sweetaleart'
    };
    return this.Swal((<any>Object).assign(baseOptions)).catch(Swal.noop);
  }

  public alertToastError(content) {
    const baseOptions = {
      html: content,
      type: 'error',
      timer: 5000,
      toast: true,
      position: 'top',
      customClass: 'customer-sweetaleart'
    };
    return this.Swal((<any>Object).assign(baseOptions)).catch(Swal.noop);
  }

  public alertInputTextarea(option) {
    const baseOptions = {
      title: 'Email address',
      input: 'textarea',
      inputAttributes: {
        autocapitalize: 'off',
      },
      showCancelButton: true,
      customClass: 'customer-sweetaleart',
      heightAuto: false
    };
    return this.Swal((<any>Object).assign(baseOptions, option));
  }
}
