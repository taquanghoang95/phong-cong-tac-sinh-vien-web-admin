import {Injectable} from '@angular/core';
import {HttpService} from './http.service';
import {Observable} from 'rxjs';
import {NewsModel} from '../../shared/model/news.model';

@Injectable()
export class NewsService {

  private url = '/api/news';

  constructor(private httpService: HttpService) {
  }

  public addNews(newsModel: NewsModel): Observable<any> {
    return this.httpService.postWithToken(this.url, newsModel);
  }

  public getNews(): Observable<NewsModel[]> {
    return this.httpService.getWithToken(this.url);
  }

  public deleteNews(id: number): Observable<any> {
    return this.httpService.deleteWithToken(this.url + '/' + id);
  }

  public updateDisplayNews(id: number, display: boolean): Observable<any> {
    return this.httpService.postWithToken(this.url + '/update?id=' + id + '&display=' + display, {});
  }
}
