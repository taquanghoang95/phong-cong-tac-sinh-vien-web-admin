import { Injectable } from '@angular/core';
import {HttpService} from './http.service';
import {Observable} from 'rxjs';
import {ScholarshipModel} from '../../shared/model/scholarship.model';

@Injectable()
export class ScholarshipService {

  private url = '/api/scholarship';

  constructor(private httpService: HttpService) { }

  public addScholarship(recruitmentModel: ScholarshipModel): Observable<any> {
    return this.httpService.postWithToken(this.url, recruitmentModel);
  }

  public getScholarship(): Observable<ScholarshipModel[]> {
    return this.httpService.getWithToken(this.url);
  }

  public deleteScholarship(id: number): Observable<any> {
    return this.httpService.deleteWithToken(this.url + '/' + id);
  }

  public updateDisplayScholarship(id: number, display: boolean): Observable<any> {
    return this.httpService.postWithToken(this.url + '/update?id=' + id + '&display=' + display, {});
  }
}
