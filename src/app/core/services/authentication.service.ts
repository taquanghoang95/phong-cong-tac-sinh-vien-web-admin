import {Injectable} from '@angular/core';
import {HttpService} from './http.service';
import {UserModel} from '../../shared/model/user.model';
import {Observable} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {Router} from '@angular/router';
import * as jwt_decode from 'jwt-decode';

@Injectable()
export class AuthenticationService {
  private url = '/api/auth';

  constructor(private http: HttpService,
              private router: Router) {
  }

  public signin(userModel: UserModel): Observable<any> {
    return this.http.post(this.url + '/signin', userModel).pipe(
      catchError(this.http.handleError)
    );
  }

  public logout(): void {
    localStorage.clear();
    this.router.navigate(['login']);
  }

  public isLogin(): boolean {
    return localStorage.getItem('token') !== null;
  }

  public isAdmin(): boolean {
    const token = jwt_decode(localStorage.getItem('token'));
    return token.role === 'ROLE_ADMIN';
  }

  public isUser(): boolean {
    const token = jwt_decode(localStorage.getItem('token'));
    return token.role === 'ROLE_USER';
  }

  public getUserId(): number {
    const token = jwt_decode(localStorage.getItem('token'));
    return token.sub;
  }
}
