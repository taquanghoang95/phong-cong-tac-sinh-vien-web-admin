import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {throwError} from 'rxjs';

@Injectable()
export class HttpService {

  constructor(private http: HttpClient) {
  }

  private prepareAuthorization() {
    const token = localStorage.getItem('token');
    const headers = new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set('Authorization', 'Bearer ' + token);
    return headers;
  }

  public post(url, data): any {
    return this.http.post(url, data);
  }

  public getWithToken(url): any {
    return this.http.get(url, {headers: this.prepareAuthorization()});
  }

  public postWithToken(url, data): any {
    return this.http.post(url, data, {headers: this.prepareAuthorization()});
  }

  public putWithToken(url, data): any {
    return this.http.put(url, data, {headers: this.prepareAuthorization()});
  }

  public deleteWithToken(url): any {
    return this.http.delete(url, {headers: this.prepareAuthorization()});
  }

  public handleError(error: HttpErrorResponse) {
    return throwError(error);
  }
}
