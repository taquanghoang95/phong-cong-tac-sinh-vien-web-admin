import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Inject, Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {APP_CONFIG, AppConfig} from '../../config/app.config';

@Injectable()
export class ApiInterceptor implements HttpInterceptor {
  constructor(@Inject(APP_CONFIG) private config: AppConfig) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let apiReq = req;
    if (req.url.startsWith('/') && !req.url.startsWith('/assets')) {
      apiReq = req.clone({url: `${this.config.API_ROOT}${req.url}`});
    }

    return next.handle(apiReq);
  }
}
