import {Component, Inject, OnInit} from '@angular/core';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {NotifyService} from '../../../core/services/notify.service';
import {SweetAlertService} from '../../../core/services/sweet-alert.service';
import {Router} from '@angular/router';
import {APP_CONFIG, AppConfig} from '../../../config/app.config';
import {editorConfig} from '../../../shared/util/util';
import {DatePipe} from '@angular/common';

@Component({
  selector: 'app-notify-create',
  templateUrl: './notify-create.component.html',
  styleUrls: ['./notify-create.component.scss'],
  providers: [DatePipe]
})
export class NotifyCreateComponent implements OnInit {
  public notifyForm: FormGroup;
  public links: FormArray;
  public images: FormArray;
  public imageURL: string;
  public imageNames = [];
  public submitted = false;
  public loading = false;
  public events = [];

  public editorConfig = editorConfig(this.config.API_ROOT);

  constructor(private fb: FormBuilder,
              private notifyService: NotifyService,
              private sweetAlertService: SweetAlertService,
              private router: Router,
              @Inject(APP_CONFIG) private config: AppConfig,
              private datePipe: DatePipe) {
  }

  ngOnInit() {
    this.notifyForm = this.fb.group({
      title: ['', Validators.required],
      description: [''],
      content: [''],
      type: ['HOT'],
      links: this.fb.array([]),
      images: this.fb.array([]),
      createdDate: [this.datePipe.transform(new Date(), 'yyyy-MM-ddTHH:mm')],
      display: [false]
    });
  }

  public createLink(): FormGroup {
    return this.fb.group({
      caption: [''],
      path: ['']
    });
  }

  public addLink(): void {
    this.links = this.notifyForm.get('links') as FormArray;
    this.links.push(this.createLink());
  }

  public removeLink(index: number): void {
    this.links.removeAt(index);
  }

  public createImage(): FormGroup {
    return this.fb.group({
      caption: [''],
      path: ['']
    });
  }

  public addImage(): void {
    this.images = this.notifyForm.get('images') as FormArray;
    this.images.push(this.createImage());
  }

  public removeImage(index: number): void {
    this.images.removeAt(index);
  }

  public submitForm(): void {
    this.submitted = true;
    if (this.events.length > 0) {
      this.events.forEach((e, index) => this.handleFileChange(e, index));
    }
    setTimeout(() => {
      if (this.notifyForm.valid) {
        this.loading = true;
        this.notifyService.addNotify(this.notifyForm.value).subscribe(response => {
            this.sweetAlertService.alertSuccess('Bạn đã tạo thông báo thành công!');
            this.loading = false;
            this.router.navigate(['/notify-list']);
          },
          error => {
            this.loading = false;
            switch (error.status) {
              case 500:
                this.sweetAlertService.alertError('Tạo thông báo bị lỗi. Vui lòng thử lại!');
                break;
              case 400:
                this.sweetAlertService.alertError('Tạo thông báo bị lỗi. Vui lòng thử lại!');
                break;
            }
          });
      }
    }, 1000);
  }

  public onFileChange(event) {
    this.events.push(event);
    const file = event.target.files[0];
    this.imageNames.push(file.name);
  }

  public handleFileChange(event, index) {
    const reader = new FileReader();
    const file = event.target.files[0];

    reader.readAsDataURL(file);
    reader.onload = () => {
      this.imageURL = file.name + ',' + file.type + ',' + file.size + ',' + (reader.result as string).split(',')[1];
      this.notifyForm.value.images[index].path = this.imageURL;
    };
  }

  get title() {
    return this.notifyForm.get('title');
  }
}
