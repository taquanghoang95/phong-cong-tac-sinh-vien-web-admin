import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NotifyCreateComponent } from './notify-create.component';

describe('NotifyCreateComponent', () => {
  let component: NotifyCreateComponent;
  let fixture: ComponentFixture<NotifyCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NotifyCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NotifyCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
