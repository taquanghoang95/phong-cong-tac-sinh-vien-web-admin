import {Component, OnInit} from '@angular/core';
import {NotifyService} from '../../../core/services/notify.service';
import {SweetAlertService} from '../../../core/services/sweet-alert.service';
import {AuthenticationService} from '../../../core/services/authentication.service';
import {NotifyModel} from '../../../shared/model/notify.model';

@Component({
  selector: 'app-notify-list',
  templateUrl: './notify-list.component.html',
  styleUrls: ['./notify-list.component.scss']
})
export class NotifyListComponent implements OnInit {
  public notifies: NotifyModel[] = [];
  public notifiesDisplay: NotifyModel[] = [];
  public loading = false;
  public page = 1;

  constructor(private notifyService: NotifyService,
              private sweetAlertService: SweetAlertService,
              private authenticationService: AuthenticationService) {
  }

  ngOnInit() {
    this.getNotifies();
  }

  private getNotifies(): void {
    this.loading = true;
    this.notifyService.getNotifies().subscribe(res => {
        this.loading = false;
        this.notifies = res;
        this.pageChange(this.page);
      },
      error => {
        this.loading = false;
        switch (error.status) {
          case 401:
            this.authenticationService.logout();
            break;
          default:
            this.sweetAlertService.alertError('Có điều gì đó không đúng. Vui lòng thử lại!');
        }
      });
  }

  public deleteNotify(id: number): void {
    const options = {
      title: 'Bạn có muốn xóa thông tin này không?',
      confirmButtonText: 'Xóa',
      confirmButtonColor: '#dc3545'
    };
    this.sweetAlertService.alertQuestion(options);
    this.sweetAlertService.alertConfirm(options).then(result => {
      if (result.value) {
        this.notifyService.deleteNotify(id).subscribe(() => {
            this.sweetAlertService.alertSuccess('Bạn đã xóa thành công!');
            this.page = 1;
            this.getNotifies();
          },
          error => {
            switch (error.status) {
              case 401:
                this.authenticationService.logout();
                break;
              default:
                this.sweetAlertService.alertError('Có điều gì đó không đúng. Vui lòng thử lại!');
            }
          });
      }
    });
  }

  public pageChange(page: number): void {
    this.page = page;
    this.notifiesDisplay = this.notifies.slice((page - 1) * 10, page * 10);
  }

  public updateDisplayNotify(id: any, display: boolean) {
    this.notifyService.updateDisplayNotify(id, !display).subscribe(res => {
      this.getNotifies();
    });
  }

}
