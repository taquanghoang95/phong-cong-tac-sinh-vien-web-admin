import {Component, Inject, OnInit} from '@angular/core';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {APP_CONFIG, AppConfig} from '../../../config/app.config';
import {SweetAlertService} from '../../../core/services/sweet-alert.service';
import {Router} from '@angular/router';
import {NewsService} from '../../../core/services/news.service';
import {editorConfig} from '../../../shared/util/util';
import {DatePipe} from '@angular/common';
import {AuthenticationService} from '../../../core/services/authentication.service';

@Component({
  selector: 'app-news-create',
  templateUrl: './news-create.component.html',
  styleUrls: ['./news-create.component.scss'],
  providers: [DatePipe]
})
export class NewsCreateComponent implements OnInit {
  public newsForm: FormGroup;
  public links: FormArray;
  public imageName: string;
  public imageURL: string;
  public submitted = false;
  public event: any;
  public loading = false;

  public editorConfig = editorConfig(this.config.API_ROOT);

  constructor(private fb: FormBuilder,
              private newsService: NewsService,
              private sweetAlertService: SweetAlertService,
              private router: Router,
              @Inject(APP_CONFIG) private config: AppConfig,
              private datePipe: DatePipe,
              private authenticationService: AuthenticationService) {
  }

  ngOnInit() {
    this.newsForm = this.fb.group({
      title: ['', Validators.required],
      description: ['', Validators.required],
      content: ['', Validators.required],
      type: ['HOT'],
      links: this.fb.array([]),
      image: ['', Validators.required],
      createdDate: [this.datePipe.transform(Date.now(), 'yyyy-MM-ddTHH:mm')],
      display: [false]
    });
  }

  public createLink(): FormGroup {
    return this.fb.group({
      caption: [''],
      path: ['']
    });
  }

  public addLink(): void {
    this.links = this.newsForm.get('links') as FormArray;
    this.links.push(this.createLink());
  }

  public removeLink(index: number): void {
    this.links.removeAt(index);
  }

  public submitForm(): void {
    this.submitted = true;
    this.handleFile(this.event);
    setTimeout(() => {
      if (this.newsForm.valid) {
        this.loading = true;
        this.newsService.addNews(this.newsForm.value).subscribe(response => {
            this.sweetAlertService.alertSuccess('Bạn đã tạo tin tức thành công!');
            this.loading = false;
            this.router.navigate(['/news-list']);
          },
          error => {
            this.loading = false;
            switch (error.status) {
              case 401:
                this.authenticationService.logout();
                break;
              case 400:
                this.sweetAlertService.alertError('Bạn tạo tin tức không thành công!');
                break;
              default:
                this.sweetAlertService.alertError('Có điều gì đó không đúng. Vui lòng thử lại!');
            }
          });
      }
    }, 1000);
  }

  public onFileChange(event) {
    this.event = event;
    const file = event.target.files[0];
    this.imageName = file.name;
  }

  public handleFile(event) {
    const reader = new FileReader();
    if (event.target.files && event.target.files.length) {
      const file = event.target.files[0];
      reader.readAsDataURL(file);
      reader.onload = () => {
        this.imageURL = file.name + ',' + file.type + ',' + file.size + ',' + (reader.result as string).split(',')[1];
        this.newsForm.value.image = this.imageURL;
      };
    }
  }

  get title() {
    return this.newsForm.get('title');
  }

  get content() {
    return this.newsForm.get('content');
  }

  get image() {
    return this.newsForm.get('image');
  }

  get description() {
    return this.newsForm.get('description');
  }
}
