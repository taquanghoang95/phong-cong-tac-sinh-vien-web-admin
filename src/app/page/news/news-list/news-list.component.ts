import {Component, OnInit} from '@angular/core';
import {NewsService} from '../../../core/services/news.service';
import {SweetAlertService} from '../../../core/services/sweet-alert.service';
import {AuthenticationService} from '../../../core/services/authentication.service';
import {NewsModel} from '../../../shared/model/news.model';

@Component({
  selector: 'app-news-list',
  templateUrl: './news-list.component.html',
  styleUrls: ['./news-list.component.scss']
})
export class NewsListComponent implements OnInit {
  public news: NewsModel[] = [];
  public newsDisplay: NewsModel[] = [];
  public loading = false;
  public page = 1;

  constructor(private newsService: NewsService,
              private sweetAlertService: SweetAlertService,
              private authenticationService: AuthenticationService) {
  }

  ngOnInit() {
    this.getNews();
  }

  private getNews(): void {
    this.loading = true;
    this.newsService.getNews().subscribe(res => {
        this.loading = false;
        this.news = res;
        this.pageChange(this.page);
      },
      error => {
        this.loading = false;
        switch (error.status) {
          case 401:
            this.authenticationService.logout();
            break;
          default:
            this.sweetAlertService.alertError('Có điều gì đó không đúng. Vui lòng thử lại!');
        }
      });
  }

  public deleteNews(id: number): void {
    const options = {
      title: 'Bạn có muốn xóa tin tức này không?',
      confirmButtonText: 'Xóa',
      confirmButtonColor: '#dc3545'
    };
    this.sweetAlertService.alertQuestion(options);
    this.sweetAlertService.alertConfirm(options).then(result => {
      if (result.value) {
        this.newsService.deleteNews(id).subscribe(() => {
            this.sweetAlertService.alertSuccess('Bạn đã xóa tin tức thành công!');
            this.page = 1;
            this.getNews();
          },
          error => {
            switch (error.status) {
              case 401:
                this.authenticationService.logout();
                break;
              default:
                this.sweetAlertService.alertError('Có điều gì đó không đúng. Vui lòng thử lại!');
            }
          });
      }
    });
  }

  public pageChange(page: number) {
    this.page = page;
    this.newsDisplay = this.news.slice((page - 1) * 10, page * 10);
  }

  public updateDisplayNews(id: number, display: boolean) {
    this.newsService.updateDisplayNews(id, !display).subscribe(res => {
      this.getNews();
    });
  }
}
