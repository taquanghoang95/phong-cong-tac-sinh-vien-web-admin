import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {LoginComponent} from './login/login.component';
import {NgxLoadingModule} from 'ngx-loading';
import {NotifyListComponent} from './notify/notify-list/notify-list.component';
import {NotifyCreateComponent} from './notify/notify-create/notify-create.component';
import {NewsCreateComponent} from './news/news-create/news-create.component';
import {NewsListComponent} from './news/news-list/news-list.component';
import {NewsService} from '../core/services/news.service';
import {NotifyService} from '../core/services/notify.service';
import {RouterModule} from '@angular/router';
import {NgbPaginationModule} from '@ng-bootstrap/ng-bootstrap';
import {AngularEditorModule} from '@kolkov/angular-editor';
import { RecruitmentListComponent } from './recruitment/recruitment-list/recruitment-list.component';
import { RecruitmentCreateComponent } from './recruitment/recruitment-create/recruitment-create.component';
import {RecruitmentService} from '../core/services/recruitment.service';
import { UserListComponent } from './user/user-list/user-list.component';
import { UserCreateComponent } from './user/user-create/user-create.component';
import {UserService} from '../core/services/user.service';
import { ScholarshipCreateComponent } from './scholarship/scholarship-create/scholarship-create.component';
import { ScholarshipListComponent } from './scholarship/scholarship-list/scholarship-list.component';
import { RecruitmentDetailComponent } from './recruitment/recruitment-detail/recruitment-detail.component';

@NgModule({
  declarations: [
    NotifyListComponent,
    NotifyCreateComponent,
    NewsListComponent,
    NewsCreateComponent,
    LoginComponent,
    RecruitmentListComponent,
    RecruitmentCreateComponent,
    UserListComponent,
    UserCreateComponent,
    ScholarshipCreateComponent,
    ScholarshipListComponent,
    RecruitmentDetailComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    AngularEditorModule,
    NgbPaginationModule,
    ReactiveFormsModule,
    NgxLoadingModule.forRoot({backdropBorderRadius: '3px', fullScreenBackdrop: true})
  ],
  providers: [
    NewsService,
    NotifyService,
    RecruitmentService,
    UserService
  ]
})
export class PageModule {
}
