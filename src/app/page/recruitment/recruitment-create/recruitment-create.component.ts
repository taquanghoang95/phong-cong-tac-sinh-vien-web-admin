import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {SweetAlertService} from '../../../core/services/sweet-alert.service';
import {Router} from '@angular/router';
import {APP_CONFIG, AppConfig} from '../../../config/app.config';
import {RecruitmentService} from '../../../core/services/recruitment.service';
import {editorConfig} from 'src/app/shared/util/util';
import {DatePipe} from '@angular/common';
import {AuthenticationService} from '../../../core/services/authentication.service';

@Component({
  selector: 'app-recruitment-create',
  templateUrl: './recruitment-create.component.html',
  styleUrls: ['./recruitment-create.component.scss'],
  providers: [DatePipe]
})
export class RecruitmentCreateComponent implements OnInit {
  public recruitmentForm: FormGroup;
  public logoName: string;
  public logoURL: string;
  public submitted = false;
  public event: any;
  public loading = false;

  public editorConfig = editorConfig(this.config.API_ROOT);

  constructor(private fb: FormBuilder,
              private recruitmentService: RecruitmentService,
              private sweetAlertService: SweetAlertService,
              private router: Router,
              @Inject(APP_CONFIG) private config: AppConfig,
              private datePipe: DatePipe,
              private authenticationService: AuthenticationService) {
  }

  ngOnInit() {
    this.recruitmentForm = this.fb.group({
      title: ['', Validators.required],
      company: ['', Validators.required],
      description: [''],
      address: ['', Validators.required],
      salary: ['', Validators.required],
      logo: [''],
      content: ['', Validators.required],
      amount: ['', Validators.required],
      phoneNumber: ['', Validators.required],
      email: ['', Validators.required],
      type: ['HOT'],
      createdDate: [this.datePipe.transform(new Date(), 'yyyy-MM-ddTHH:mm')],
      expiredDate: ['', Validators.required],
      display: [false]
    });
  }

  public onFileChange(event) {
    this.event = event;
    const file = event.target.files[0];
    this.logoName = file.name;
  }

  public handleFile(event): void {
    const reader = new FileReader();
    const file = event.target.files[0];
    reader.readAsDataURL(file);
    reader.onload = () => {
      this.logoURL = file.name + ',' + file.type + ',' + file.size + ',' + (reader.result as string).split(',')[1];
      this.recruitmentForm.value.logo = this.logoURL;
    };
  }

  public submitForm(): void {
    this.submitted = true;
    this.handleFile(this.event);
    setTimeout(() => {
      if (this.recruitmentForm.valid) {
        this.loading = true;
        this.recruitmentService.addRecruitment(this.recruitmentForm.value).subscribe(response => {
            this.sweetAlertService.alertSuccess('Bạn đã tạo tin tuyển dụng thành công!');
            this.loading = false;
            this.router.navigate(['/recruitment-list']);
          },
          error => {
            this.loading = false;
            switch (error.status) {
              case 401:
                this.authenticationService.logout();
                break;
              case 400:
                this.sweetAlertService.alertError('Bạn tạo truyển dụng không thành công!');
                break;
              default:
                this.sweetAlertService.alertError('Có điều gì đó không đúng. Vui lòng thử lại!');
            }
          });
      }
    }, 1000);
  }

  get title() {
    return this.recruitmentForm.get('title');
  }

  get company() {
    return this.recruitmentForm.get('company');
  }

  get address() {
    return this.recruitmentForm.get('address');
  }

  get phoneNumber() {
    return this.recruitmentForm.get('phoneNumber');
  }

  get email() {
    return this.recruitmentForm.get('email');
  }

  get salary() {
    return this.recruitmentForm.get('salary');
  }

  get amount() {
    return this.recruitmentForm.get('amount');
  }

  get content() {
    return this.recruitmentForm.get('content');
  }

  get expiredDate() {
    return this.recruitmentForm.get('expiredDate');
  }
}
