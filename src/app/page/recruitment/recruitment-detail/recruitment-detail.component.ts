import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {RecruitmentService} from '../../../core/services/recruitment.service';
import {RecruitmentModel} from '../../../shared/model/recruitment.model';

@Component({
  selector: 'app-recruitment-detail',
  templateUrl: './recruitment-detail.component.html',
  styleUrls: ['./recruitment-detail.component.scss']
})
export class RecruitmentDetailComponent implements OnInit {
  public recruitment: RecruitmentModel;

  constructor(private route: ActivatedRoute,
              private recruitmentService: RecruitmentService) {
  }

  ngOnInit() {
    this.getRecruitment();
  }

  public getRecruitment(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.recruitmentService.getRecruitmentById(id).subscribe(res => {
        this.recruitment = res;
      });
  }
}
