import {Component, OnInit} from '@angular/core';
import {RecruitmentService} from '../../../core/services/recruitment.service';
import {SweetAlertService} from '../../../core/services/sweet-alert.service';
import {AuthenticationService} from '../../../core/services/authentication.service';
import {RecruitmentModel} from '../../../shared/model/recruitment.model';
import {Router} from '@angular/router';

@Component({
  selector: 'app-recruitment-list',
  templateUrl: './recruitment-list.component.html',
  styleUrls: ['./recruitment-list.component.scss']
})
export class RecruitmentListComponent implements OnInit {
  public recruitment: RecruitmentModel[] = [];
  public recruitmentDisplay: RecruitmentModel[] = [];
  public loading = false;
  public page = 1;

  constructor(private recruitmentService: RecruitmentService,
              private sweetAlertService: SweetAlertService,
              private authenticationService: AuthenticationService,
              private router: Router) {
  }

  ngOnInit() {
    this.getRecruitment();
  }

  private getRecruitment(): void {
    this.loading = true;
    this.recruitmentService.getRecruitment().subscribe(res => {
        this.loading = false;
        this.recruitment = res;
        this.pageChange(this.page);
      },
      error => {
        this.loading = false;
        switch (error.status) {
          case 401:
            this.authenticationService.logout();
            break;
          default:
            this.sweetAlertService.alertError('Có điều gì đó không đúng. Vui lòng thử lại!');
        }
      });
  }

  public pageChange(page: number): void {
    this.page = page;
    this.recruitmentDisplay = this.recruitment.slice((page - 1) * 10, page * 10);
  }

  public deleteRecruitment(id: number): void {
    const options = {
      title: 'Bạn có muốn xóa tin tuyển dụng này không?',
      confirmButtonText: 'Xóa',
      confirmButtonColor: '#dc3545'
    };
    this.sweetAlertService.alertQuestion(options);
    this.sweetAlertService.alertConfirm(options).then(result => {
      if (result.value) {
        this.recruitmentService.deleteRecruitment(id).subscribe(() => {
            this.sweetAlertService.alertSuccess('Bạn đã xóa thành công!');
            this.page = 1;
            this.getRecruitment();
          },
          error => {
            switch (error.status) {
              case 401:
                this.authenticationService.logout();
                break;
              default:
                this.sweetAlertService.alertError('Có điều gì đó không đúng. Vui lòng thử lại!');
            }
          });
      }
    });
  }

  public updateDisplayRecruitment(id: any, display: boolean) {
    this.recruitmentService.updateDisplayRecruitment(id, !display).subscribe(res => {
      this.getRecruitment();
    });
  }

  public navigateDetail(id: number) {
    return this.router.navigate(['recruitment-list/' + id]);
  }
}
