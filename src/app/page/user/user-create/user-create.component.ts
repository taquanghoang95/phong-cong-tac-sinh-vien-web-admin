import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {UserService} from '../../../core/services/user.service';
import {SweetAlertService} from '../../../core/services/sweet-alert.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-user-create',
  templateUrl: './user-create.component.html',
  styleUrls: ['./user-create.component.scss']
})
export class UserCreateComponent implements OnInit {

  public userForm: FormGroup;
  public submitted = false;
  public loading = false;

  constructor(private fb: FormBuilder,
              private userService: UserService,
              private sweetAlertService: SweetAlertService,
              private router: Router) {
  }

  ngOnInit() {
    this.userForm = this.fb.group({
      fullname: ['', Validators.required],
      username: ['', Validators.required],
      email: ['', Validators.required],
      password: ['', Validators.required],
      roleName: ['ROLE_USER'],
      department: [''],
      position: [''],
      gender: [''],
      phoneNumber: [''],
      birthday: ['']
    });
  }

  submitForm() {
    this.submitted = true;
    if (this.userForm.valid) {
      this.loading = true;
      this.userService.addUser(this.userForm.value).subscribe(response => {
          this.loading = false;
          this.sweetAlertService.alertSuccess('Bạn đã tạo tài khoản thành công!');
          this.router.navigate(['/user-list']);
        },
        error => {
          switch (error.status) {
            case 459:
              this.sweetAlertService.alertError('Tài khoản đã tồn tại.');
              break;
            case 460:
              this.sweetAlertService.alertError('Email đã tồn tại.');
              break;
          }
        }
      );
    }
  }

  get fullname() {
    return this.userForm.get('fullname');
  }

  get username() {
    return this.userForm.get('username');
  }

  get email() {
    return this.userForm.get('email');
  }

  get password() {
    return this.userForm.get('password');
  }
}
