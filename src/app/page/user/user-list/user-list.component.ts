import {Component, OnInit} from '@angular/core';
import {SweetAlertService} from '../../../core/services/sweet-alert.service';
import {AuthenticationService} from '../../../core/services/authentication.service';
import {UserService} from '../../../core/services/user.service';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit {
  public users = [];
  public usersDisplay = [];
  public loading = false;
  public page = 1;

  constructor(private userService: UserService,
              private sweetAlertService: SweetAlertService,
              private authenticationService: AuthenticationService) {
  }

  ngOnInit() {
    this.getUsers();
  }

  public getUsers(): void {
    this.loading = true;
    this.userService.getUsers().subscribe(res => {
        this.loading = false;
        this.users = res;
        this.pageChange(this.page);
      },
      error => {
        this.loading = false;
        switch (error.status) {
          case 401:
            this.authenticationService.logout();
            break;
          default:
            this.sweetAlertService.alertError('Có điều gì đó không đúng. Vui lòng thử lại!');
        }
      });
  }

  public pageChange(page: number): void {
    this.page = page;
    this.usersDisplay = this.users.slice((page - 1) * 10, page * 10);
  }

  deleteUser(id: number): void {
    const options = {
      title: 'Bạn có muốn xóa tài khoản này không?',
      confirmButtonText: 'Xóa',
      confirmButtonColor: '#dc3545'
    };
    this.sweetAlertService.alertQuestion(options);
    this.sweetAlertService.alertConfirm(options).then(result => {
      if (result.value) {
        this.userService.deleteUser(id).subscribe(() => {
          this.sweetAlertService.alertSuccess('Bạn đã xóa thành công!');
          this.page = 1;
          this.getUsers();
        });
      }
    });
  }
}
