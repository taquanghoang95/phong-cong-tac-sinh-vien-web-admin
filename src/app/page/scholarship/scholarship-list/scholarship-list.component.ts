import {Component, OnInit} from '@angular/core';
import {ScholarshipModel} from '../../../shared/model/scholarship.model';
import {SweetAlertService} from '../../../core/services/sweet-alert.service';
import {AuthenticationService} from '../../../core/services/authentication.service';
import {ScholarshipService} from '../../../core/services/scholarship.service';

@Component({
  selector: 'app-scholarship-list',
  templateUrl: './scholarship-list.component.html',
  styleUrls: ['./scholarship-list.component.scss']
})
export class ScholarshipListComponent implements OnInit {
  public scholarships: ScholarshipModel[] = [];
  public scholarshipsDisplay: ScholarshipModel[] = [];
  public loading = false;
  public page = 1;

  constructor(private scholarshipService: ScholarshipService,
              private sweetAlertService: SweetAlertService,
              private authenticationService: AuthenticationService) {
  }

  ngOnInit() {
    this.getScholarships();
  }

  private getScholarships(): void {
    this.loading = true;
    this.scholarshipService.getScholarship().subscribe(res => {
        this.loading = false;
        this.scholarships = res;
        this.pageChange(this.page);
      },
      error => {
        this.loading = false;
        switch (error.status) {
          case 401:
            this.authenticationService.logout();
            break;
          default:
            this.sweetAlertService.alertError('Có điều gì đó không đúng. Vui lòng thử lại!');
        }
      });
  }

  public deleteScholarship(id: number): void {
    const options = {
      title: 'Bạn có muốn xóa học bổng này không?',
      confirmButtonText: 'Xóa',
      confirmButtonColor: '#dc3545'
    };
    this.sweetAlertService.alertQuestion(options);
    this.sweetAlertService.alertConfirm(options).then(result => {
      if (result.value) {
        this.scholarshipService.deleteScholarship(id).subscribe(() => {
            this.sweetAlertService.alertSuccess('Bạn đã xóa thành công!');
            this.page = 1;
            this.getScholarships();
          },
          error => {
            switch (error.status) {
              case 401:
                this.authenticationService.logout();
                break;
              default:
                this.sweetAlertService.alertError('Có điều gì đó không đúng. Vui lòng thử lại!');
            }
          });
      }
    });
  }

  public pageChange(page: number): void {
    this.page = page;
    this.scholarshipsDisplay = this.scholarships.slice((page - 1) * 10, page * 10);
  }

  public updateDisplayScholarship(id: any, display: boolean) {
    this.scholarshipService.updateDisplayScholarship(id, !display).subscribe(res => {
      this.getScholarships();
    });
  }
}
