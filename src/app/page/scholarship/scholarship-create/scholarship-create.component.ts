import {Component, Inject, OnInit} from '@angular/core';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ScholarshipService} from '../../../core/services/scholarship.service';
import {SweetAlertService} from '../../../core/services/sweet-alert.service';
import {Router} from '@angular/router';
import {APP_CONFIG, AppConfig} from '../../../config/app.config';
import {editorConfig} from '../../../shared/util/util';
import {DatePipe} from '@angular/common';

@Component({
  selector: 'app-scholarship-create',
  templateUrl: './scholarship-create.component.html',
  styleUrls: ['./scholarship-create.component.scss'],
  providers: [DatePipe]
})
export class ScholarshipCreateComponent implements OnInit {
  public scholarshipForm: FormGroup;
  public links: FormArray;
  public images: FormArray;
  public imageURL: string;
  public imageNames = [];
  public submitted = false;
  public loading = false;
  public events = [];

  public editorConfig = editorConfig(this.config.API_ROOT);

  constructor(private scholarshipService: ScholarshipService,
              private fb: FormBuilder,
              private sweetAlertService: SweetAlertService,
              private router: Router,
              @Inject(APP_CONFIG) private config: AppConfig,
              private datePipe: DatePipe) {
  }

  ngOnInit() {
    this.scholarshipForm = this.fb.group({
      title: ['', Validators.required],
      description: [''],
      type: ['', Validators.required],
      amount: ['', Validators.required],
      level: ['', Validators.required],
      information: ['', Validators.required],
      links: this.fb.array([]),
      images: this.fb.array([]),
      createdDate: [this.datePipe.transform(new Date(), 'yyyy-MM-ddTHH:mm')],
      expiredDate: ['', Validators.required],
      display: [false]
    });
  }

  public createLink(): FormGroup {
    return this.fb.group({
      caption: [''],
      path: ['']
    });
  }

  public addLink(): void {
    this.links = this.scholarshipForm.get('links') as FormArray;
    this.links.push(this.createLink());
  }

  public removeLink(index: number): void {
    this.links.removeAt(index);
  }

  public createImage(): FormGroup {
    return this.fb.group({
      caption: [''],
      path: ['']
    });
  }

  public addImage(): void {
    this.images = this.scholarshipForm.get('images') as FormArray;
    this.images.push(this.createImage());
  }

  public removeImage(index: number): void {
    this.images.removeAt(index);
  }

  public submitForm(): void {
    this.submitted = true;
    this.events.forEach((e, index) => this.handleFileChange(e, index));
    setTimeout(() => {
      if (this.scholarshipForm.valid) {
        this.loading = true;
        this.scholarshipService.addScholarship(this.scholarshipForm.value).subscribe(response => {
            this.sweetAlertService.alertSuccess('Bạn đã tạo học bổng thành công!');
            this.loading = false;
            this.router.navigate(['/scholarship-list']);
          },
          error => {
            this.loading = false;
            switch (error.status) {
              case 500:
                this.sweetAlertService.alertError('Tạo học bổng bị lỗi. Vui lòng thử lại!');
            }
          }
        );
      }
    }, 1000);
  }

  public onFileChange(event) {
    this.events.push(event);
    const file = event.target.files[0];
    this.imageNames.push(file.name);
  }

  public handleFileChange(event, index) {
    const reader = new FileReader();
    const file = event.target.files[0];

    reader.readAsDataURL(file);
    reader.onload = () => {
      this.imageURL = file.name + ',' + file.type + ',' + file.size + ',' + (reader.result as string).split(',')[1];
      this.scholarshipForm.value.images[index].path = this.imageURL;
    };
  }

  get title() {
    return this.scholarshipForm.get('title');
  }

  get type() {
    return this.scholarshipForm.get('type');
  }

  get amount() {
    return this.scholarshipForm.get('amount');
  }

  get level() {
    return this.scholarshipForm.get('level');
  }

  get information() {
    return this.scholarshipForm.get('information');
  }

  get expiredDate() {
    return this.scholarshipForm.get('expiredDate');
  }
}
