import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {AuthenticationService} from '../../core/services/authentication.service';
import {Router} from '@angular/router';
import {SweetAlertService} from '../../core/services/sweet-alert.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  public formSignin: FormGroup;
  public loading = false;

  constructor(private fb: FormBuilder,
              private router: Router,
              private authenticationService: AuthenticationService,
              private sweetAlertService: SweetAlertService) {
      if (this.authenticationService.isLogin()) {
        this.router.navigate(['/']);
      }
  }

  ngOnInit() {
    this.formSignin = this.fb.group({
      usernameOrEmail: [''],
      password: ['']
    });
  }

  public submitForm(): void {
    this.loading = true;
    this.authenticationService.signin(this.formSignin.value).subscribe(response => {
      this.loading = false;
      localStorage.setItem('token', response.accessToken);
      this.router.navigate(['']);
    }, error => {
      switch (error.status) {
        case 401:
          this.sweetAlertService.alertError('Sai thông tin đăng nhập!');
      }
      this.loading = false;
    });
  }

}
