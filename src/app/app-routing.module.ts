import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {NotifyListComponent} from './page/notify/notify-list/notify-list.component';
import {NotifyCreateComponent} from './page/notify/notify-create/notify-create.component';
import {LoginComponent} from './page/login/login.component';
import {AuthGuard} from './core/authentication/auth-guard.service';
import {NewsListComponent} from './page/news/news-list/news-list.component';
import {NewsCreateComponent} from './page/news/news-create/news-create.component';
import {RecruitmentListComponent} from './page/recruitment/recruitment-list/recruitment-list.component';
import {RecruitmentCreateComponent} from './page/recruitment/recruitment-create/recruitment-create.component';
import {UserListComponent} from './page/user/user-list/user-list.component';
import {UserCreateComponent} from './page/user/user-create/user-create.component';
import {ScholarshipListComponent} from './page/scholarship/scholarship-list/scholarship-list.component';
import {ScholarshipCreateComponent} from './page/scholarship/scholarship-create/scholarship-create.component';
import {RecruitmentDetailComponent} from './page/recruitment/recruitment-detail/recruitment-detail.component';

const routes: Routes = [
  {path: 'login', component: LoginComponent},
  {path: '', pathMatch: 'full', redirectTo: 'notify-list'},
  {path: 'notify-list', component: NotifyListComponent, canActivate: [AuthGuard]},
  {path: 'notify-create', component: NotifyCreateComponent, canActivate: [AuthGuard]},
  {path: 'news-list', component: NewsListComponent, canActivate: [AuthGuard]},
  {path: 'news-create', component: NewsCreateComponent, canActivate: [AuthGuard]},
  {path: 'recruitment-list', component: RecruitmentListComponent, canActivate: [AuthGuard]},
  {path: 'recruitment-create', component: RecruitmentCreateComponent, canActivate: [AuthGuard]},
  {path: 'recruitment-list/:id', component: RecruitmentDetailComponent, canActivate: [AuthGuard]},
  {path: 'scholarship-list', component: ScholarshipListComponent, canActivate: [AuthGuard]},
  {path: 'scholarship-create', component: ScholarshipCreateComponent, canActivate: [AuthGuard]},
  {path: 'user-list', component: UserListComponent, canActivate: [AuthGuard]},
  {path: 'user-create', component: UserCreateComponent, canActivate: [AuthGuard]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
